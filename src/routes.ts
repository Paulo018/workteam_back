import {UserController} from "./controller/TblUserController";
import {PersonController} from "./controller/TblPersonController";
import {TagController} from "./controller/TblTagController";
import {ProjectController} from "./controller/TblProjectController";
import {ProjectUserController} from "./controller/TblProjectUserController";
import {ProjectTagController} from "./controller/TblProjectTagController";

export const Routes = [

    {
        method: "get",
        route: "/user",
        controller: UserController,
        action: "all"
    }, 
    {
        method: "get",
        route: "/user/:id",
        controller: UserController,
        action: "one"
    },
    {
        method: "post",
        route: "/user",
        controller: UserController,
        action: "save"
    },
    {
        method: "delete",
        route: "/user/:id",
        controller: UserController,
        action: "remove"
    },





    {
        method: "get",
        route: "/person",
        controller: PersonController,
        action: "all"
    }, 
    {
        method: "get",
        route: "/person/:id",
        controller: PersonController,
        action: "one"
    },
    {
        method: "post",
        route: "/person",
        controller: PersonController,
        action: "save"
    },
    {
        method: "delete",
        route: "/person/:id",
        controller: PersonController,
        action: "remove"
    },








    {
        method: "get",
        route: "/tag",
        controller: TagController,
        action: "all"
    }, 
    {
        method: "get",
        route: "/tag/:id",
        controller: TagController,
        action: "one"
    },
    {
        method: "post",
        route: "/tag",
        controller: TagController,
        action: "save"
    },
    {
        method: "delete",
        route: "/tag/:id",
        controller: TagController,
        action: "remove"
    },







    {
        method: "get",
        route: "/project",
        controller: ProjectController,
        action: "all"
    }, 
    {
        method: "get",
        route: "/project/:id",
        controller: ProjectController,
        action: "one"
    },
    {
        method: "post",
        route: "/project",
        controller: ProjectController,
        action: "save"
    },
    {
        method: "delete",
        route: "/project/:id",
        controller: ProjectController,
        action: "remove"
    },





    {
        method: "get",
        route: "/project-user",
        controller: ProjectUserController,
        action: "all"
    }, 
    {
        method: "get",
        route: "/project-user/:id",
        controller: ProjectUserController,
        action: "one"
    },
    {
        method: "post",
        route: "/project-user",
        controller: ProjectUserController,
        action: "save"
    },
    {
        method: "delete",
        route: "/project-user/:id",
        controller: ProjectUserController,
        action: "remove"
    },






    {
        method: "get",
        route: "/project-tag",
        controller: ProjectTagController,
        action: "all"
    }, 
    {
        method: "get",
        route: "/project-tag/:id",
        controller: ProjectTagController,
        action: "one"
    },
    {
        method: "post",
        route: "/project-tag",
        controller: ProjectTagController,
        action: "save"
    },
    {
        method: "delete",
        route: "/project-tag/:id",
        controller: ProjectTagController,
        action: "remove"
    }


];