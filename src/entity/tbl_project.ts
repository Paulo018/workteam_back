import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {tbl_project_tag} from "./tbl_project_tag";
import {tbl_project_user} from "./tbl_project_user";


@Entity()
export class tbl_project {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        name:"id"
        })
    id:string;
        

    @Column("character varying",{ 
        nullable:true,
        length:255,
        name:"name"
        })
    name:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"description"
        })
    description:string | null;
        

   
    @OneToMany(type=>tbl_project_tag, tbl_project_tag=>tbl_project_tag.project)
    tblProjectTags:tbl_project_tag[];
    

   
    @OneToMany(type=>tbl_project_user, tbl_project_user=>tbl_project_user.idProject)
    tblProjectUsers:tbl_project_user[];
    
}
