import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {tbl_project_tag} from "./tbl_project_tag";


@Entity()
export class tbl_tag {

    @PrimaryGeneratedColumn({
        type:"smallint", 
        name:"id"
        })
    id:number;
        

    @Column("character varying",{ 
        nullable:true,
        length:255,
        name:"name"
        })
    name:string | null;
        

    @Column("text",{ 
        nullable:true,
        name:"description"
        })
    description:string | null;
        

   
    @OneToMany(type=>tbl_project_tag, tbl_project_tag=>tbl_project_tag.tag)
    tblProjectTags:tbl_project_tag[];
    
}
