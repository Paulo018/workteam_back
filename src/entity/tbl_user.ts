import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId, CreateDateColumn, UpdateDateColumn} from "typeorm";
import {tbl_person} from "./tbl_person";
import {tbl_project_user} from "./tbl_project_user";


@Entity()
export class tbl_user {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        name:"id"
        })
    id:string;
        

    @Column("character varying",{ 
        nullable:true,
        length:255,
        name:"username"
        })
    username:string | null;
        

   
    @OneToMany(type=>tbl_person, tbl_person=>tbl_person.user)
    tblPersons:tbl_person[];
    

   
    @OneToMany(type=>tbl_project_user, tbl_project_user=>tbl_project_user.idUser)
    tblProjectUsers:tbl_project_user[];
    
}
