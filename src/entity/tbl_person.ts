import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {tbl_user} from "./tbl_user";


@Entity()
export class tbl_person {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        name:"id"
        })
    id:string;
        

    @Column("character varying",{ 
        nullable:true,
        length:255,
        name:"name"
        })
    name:string | null;
        

    @Column("character varying",{ 
        nullable:true,
        length:255,
        name:"description"
        })
    description:string | null;
        
    @Column("character varying",{ 
        nullable:true,
        length:255,
        name:"surname"
        })
    surname:string | null;
        

   
    @ManyToOne(type=>tbl_user, tbl_user=>tbl_user.tblPersons,{  })
    @JoinColumn({ name:'user_id'})
    user:tbl_user | null;

}
