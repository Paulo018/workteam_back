import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {tbl_project} from "./tbl_project";
import {tbl_tag} from "./tbl_tag";


@Entity()
export class tbl_project_tag {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        name:"id"
        })
    id:string;
        

   
    @ManyToOne(type=>tbl_project, tbl_project=>tbl_project.tblProjectTags,{  })
    @JoinColumn({ name:'project_id'})
    project:tbl_project | null;


   
    @ManyToOne(type=>tbl_tag, tbl_tag=>tbl_tag.tblProjectTags,{  })
    @JoinColumn({ name:'tag_id'})
    tag:tbl_tag | null;

}
