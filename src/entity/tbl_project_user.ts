import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {tbl_project} from "./tbl_project";
import {tbl_user} from "./tbl_user";


@Entity()
export class tbl_project_user {

    @PrimaryGeneratedColumn({
        type:"bigint", 
        name:"id"
        })
    id:string;
        

   
    @ManyToOne(type=>tbl_project, tbl_project=>tbl_project.tblProjectUsers,{  })
    @JoinColumn({ name:'id_project'})
    idProject:tbl_project | null;


   
    @ManyToOne(type=>tbl_user, tbl_user=>tbl_user.tblProjectUsers,{  })
    @JoinColumn({ name:'id_user'})
    idUser:tbl_user | null;

}
