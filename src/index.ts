import "reflect-metadata";
import {createConnection} from "typeorm";
import * as express from "express";
import * as bodyParser from "body-parser";
import {Request, Response} from "express";
import {Routes} from "./routes";
import {User} from "./entity/User";
import {tbl_person} from "./entity/tbl_person";
import {tbl_user} from "./entity/tbl_user";
import {tbl_project_tag} from "./entity/tbl_project_tag";
import {tbl_project_user} from "./entity/tbl_project_user";
import {tbl_project} from "./entity/tbl_project";
import {tbl_tag} from "./entity/tbl_tag";

createConnection().then(async connection => {

    // create express app
    const app = express();
    app.use(bodyParser.json());


    app.use(function(req, res, next) {

        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Credentials", true);
        res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    });

    // register express routes from defined application routes
    Routes.forEach(route => {
        (app as any)[route.method](route.route, (req: Request, res: Response, next: Function) => {
            const result = (new (route.controller as any))[route.action](req, res, next);
            if (result instanceof Promise) {
                result.then(result => result !== null && result !== undefined ? res.send(result) : undefined);

            } else if (result !== null && result !== undefined) {
                res.json(result);
            }
        });
    });

    // setup express app here
    // ...

    // start express server
    app.listen(3000);

    // insert new users for test
    await connection.manager.save(connection.manager.create(tbl_tag, {
        name: "Programación",
        description: "Programación"
    }));
    await connection.manager.save(connection.manager.create(tbl_tag, {
        name: "Salud",
        description: "Salud"
    }));
    await connection.manager.save(connection.manager.create(tbl_tag, {
        name: "Negocios",
        description: "Negocios"
    }));
    await connection.manager.save(connection.manager.create(User, {
        firstName: "Timber",
        lastName: "Saw",
        age: 27
    }));

    await connection.manager.save(connection.manager.create(User, {
        firstName: "Phantom",
        lastName: "Assassin",
        age: 24
    }));

    console.log("Servidor iniciado");

}).catch(error => console.log(error));
