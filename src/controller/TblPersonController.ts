import {getRepository} from "typeorm";
import {NextFunction, Request, Response} from "express";
import {tbl_person} from "../entity/tbl_person";

export class PersonController {

    private personRepository = getRepository(tbl_person);

    async all(request: Request, response: Response, next: NextFunction) {
        return this.personRepository.find();
    }

    async one(request: Request, response: Response, next: NextFunction) {
        return this.personRepository.findOne(request.params.id);
    }

    async save(request: Request, response: Response, next: NextFunction) {
        return this.personRepository.save(request.body);
    }

    async remove(request: Request, response: Response, next: NextFunction) {
        let userToRemove = await this.personRepository.findOne(request.params.id);
        await this.personRepository.remove(userToRemove);
    }

}