import {getRepository} from "typeorm";
import {NextFunction, Request, Response} from "express";
import {tbl_project} from "../entity/tbl_project";

export class ProjectController {

    private projectRepository = getRepository(tbl_project);

    async all(request: Request, response: Response, next: NextFunction) {
        return this.projectRepository.find();
    }

    async one(request: Request, response: Response, next: NextFunction) {
        return this.projectRepository.findOne(request.params.id);
    }

    async save(request: Request, response: Response, next: NextFunction) {
        return this.projectRepository.save(request.body);
    }

    async remove(request: Request, response: Response, next: NextFunction) {
        let userToRemove = await this.projectRepository.findOne(request.params.id);
        await this.projectRepository.remove(userToRemove);
    }

}