import {getRepository} from "typeorm";
import {NextFunction, Request, Response} from "express";
import {tbl_project_user} from "../entity/tbl_project_user";

export class ProjectUserController {

    private projectUserRepository = getRepository(tbl_project_user);

    async all(request: Request, response: Response, next: NextFunction) {
        return this.projectUserRepository.find();
    }

    async one(request: Request, response: Response, next: NextFunction) {
        return this.projectUserRepository.findOne(request.params.id);
    }

    async save(request: Request, response: Response, next: NextFunction) {
        return this.projectUserRepository.save(request.body);
    }

    async remove(request: Request, response: Response, next: NextFunction) {
        let userToRemove = await this.projectUserRepository.findOne(request.params.id);
        await this.projectUserRepository.remove(userToRemove);
    }

}