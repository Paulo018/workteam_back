import {getRepository} from "typeorm";
import {NextFunction, Request, Response} from "express";
import {tbl_tag} from "../entity/tbl_tag";

export class TagController {

    private tagRepository = getRepository(tbl_tag);

    async all(request: Request, response: Response, next: NextFunction) {
        return this.tagRepository.find();
    }

    async one(request: Request, response: Response, next: NextFunction) {
        return this.tagRepository.findOne(request.params.id);
    }

    async save(request: Request, response: Response, next: NextFunction) {
        return this.tagRepository.save(request.body);
    }

    async remove(request: Request, response: Response, next: NextFunction) {
        let userToRemove = await this.tagRepository.findOne(request.params.id);
        await this.tagRepository.remove(userToRemove);
    }

}