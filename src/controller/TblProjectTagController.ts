import {getRepository} from "typeorm";
import {NextFunction, Request, Response} from "express";
import {tbl_project_tag} from "../entity/tbl_project_tag";

export class ProjectTagController {

    private projectTagRepository = getRepository(tbl_project_tag);

    async all(request: Request, response: Response, next: NextFunction) {
        return this.projectTagRepository.find();
    }

    async one(request: Request, response: Response, next: NextFunction) {
        return this.projectTagRepository.findOne(request.params.id);
    }

    async save(request: Request, response: Response, next: NextFunction) {
        return this.projectTagRepository.save(request.body);
    }

    async remove(request: Request, response: Response, next: NextFunction) {
        let userToRemove = await this.projectTagRepository.findOne(request.params.id);
        await this.projectTagRepository.remove(userToRemove);
    }

}